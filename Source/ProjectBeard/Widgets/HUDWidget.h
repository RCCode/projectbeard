// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "../Rune/RuneProperty.h"
#include "HUDWidget.generated.h"

/** Class is abstract, as we don't want to create
* instances of this, instead we want to create instances
* of our UMG Blueprint subclass.
*/
UCLASS(Abstract)
class PROJECTBEARD_API UHUDWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TMap<ERuneGroup, class UTexture2D*> IconAssets;
	
protected:
	
	// Setup in NativeConstruct
	virtual void NativeConstruct() override;

public:

	// Animate in blueprint to hide this widget
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HideWidget();

	// Animate in blueprint to show this widget
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowWidget();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ChangeNewPickUp(FName Valuetext, FName RuneName, const ERuneGroup& RuneType);
};
