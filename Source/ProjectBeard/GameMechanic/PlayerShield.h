// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "PlayerShield.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTBEARD_API UPlayerShield : public UWeapon
{
	GENERATED_BODY()
	
public:

	UPlayerShield();

protected:

	UPROPERTY()
	class APlayerCharacter* Player;

protected:

	virtual void SetupCombatCollision() override;

	virtual void OverlapResponse(const ECollisionChannel& Response, const FVector& ImpactPoint, const EPhysicalSurface& PhysicalSurface) override;

	virtual bool OverlapIsValid(AActor* OtherActor, UPrimitiveComponent* OtherComp) override;


	virtual void BeginPlay() override;
};
