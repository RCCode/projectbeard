// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerRuneCollector.h"
#include "../Player/PlayerCharacter.h"
#include "../Rune/Rune.h"
#include "Components/CapsuleComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Math/UnrealMathUtility.h" 


UPlayerRuneCollector::UPlayerRuneCollector()
{
	PrimaryComponentTick.bCanEverTick = true;

	// Preconfigure Collision
	SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
	SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
	SetGenerateOverlapEvents(true);

	InitSphereRadius(500.f);
	LerpSpeed = 2.5f;
}

void UPlayerRuneCollector::IterateCollectedRunes(const float& DeltaTime)
{
	if (CurrentRunes.Num() <= 0)
	{
		return;
	}

	TSet<AActor*> TempSet = CurrentRunes;
	for (AActor*& CurrentRune : TempSet)
	{
		if (!CurrentRune)
		{
			continue;
		}

		if (IsValid(CurrentRune))
		{
			LerpRune(CurrentRune, DeltaTime);
		}
		else
		{
			CurrentRunes.Remove(CurrentRune);
		}
	}
}

void UPlayerRuneCollector::LerpRune(AActor* CurrentRune, const float& DeltaTime)
{
	
	float res = FMath::Clamp(((1.0f - FVector::Distance(CurrentRune->GetActorLocation(), Player->GetActorLocation()) / GetScaledSphereRadius()) * LerpSpeed) * DeltaTime, 0.f, 1.f);
	CurrentRune->SetActorLocation(
		FMath::Lerp(
			CurrentRune->GetActorLocation(),
			Player->GetActorLocation(),
			res));
	

}

void UPlayerRuneCollector::BeginPlay()
{
	Super::BeginPlay();
	// Get Player
	Player = Cast<APlayerCharacter>(GetOwner());
	OnComponentBeginOverlap.AddDynamic(this, &UPlayerRuneCollector::OnOverlapBegin);
	OnComponentEndOverlap.AddDynamic(this, &UPlayerRuneCollector::OnOverlapEnd);
}

void UPlayerRuneCollector::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (Player)
	{
		IterateCollectedRunes(DeltaTime);	
	}
}

void UPlayerRuneCollector::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!Player)
	{
		UE_LOG(LogTemp, Warning, TEXT("No Player"));
		return;
	}
	
	FString s = OtherActor->GetHumanReadableName();

	if (Cast<ARune>(OtherActor))
	{

		if (!CurrentRunes.Contains(OtherActor))
		{
			CurrentRunes.Emplace(OtherActor);
			OtherComp->SetSimulatePhysics(false);
			OtherComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Ignore);
			OnRuneDetected.Broadcast(OtherActor);
		}
	}
}

void UPlayerRuneCollector::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (Cast<ARune>(OtherActor))
	{
		if (CurrentRunes.Contains(OtherActor))
		{
			OtherComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
			OtherComp->SetSimulatePhysics(true);
			CurrentRunes.Remove(OtherActor);
		}
	}
}

