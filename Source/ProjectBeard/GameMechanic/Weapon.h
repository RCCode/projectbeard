// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PrimitiveComponent.h" 
#include "Weapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponHitCollision,const TEnumAsByte<ECollisionChannel>&, CollisionChannel, const TEnumAsByte<EPhysicalSurface>&, PhysicalSurface);

UCLASS(EditInlineNew, Blueprintable)
class PROJECTBEARD_API UWeapon : public UPrimitiveComponent
{
	GENERATED_BODY()

public:
	
	UWeapon();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Mesh)
	class UStaticMeshComponent* WeaponMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Collision, meta = (MakeEditWidget))
	class UBoxComponent* CombatCollision;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Particles)
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> CollisionParticles;

	UPROPERTY(BlueprintAssignable)
	FOnWeaponHitCollision OnWeaponHitCollision;


protected:

	virtual void BeginPlay() override;

	virtual void SetupDefaultSubObject();

	virtual void SetupRootAttachment();

	virtual void SetupCombatCollision();

	virtual void OverlapResponse(const ECollisionChannel& Response, const FVector& ImpactPoint, const EPhysicalSurface& PhysicalSurface);

	virtual void PlayCombatParticleSystem(const TEnumAsByte<EPhysicalSurface>& Response, const FVector& ImpactPoint);

	virtual bool OverlapIsValid(AActor* OtherActor, UPrimitiveComponent* OtherComp);

public:

	UFUNCTION(BlueprintCallable)
	virtual void SetCombatCollisionEnabled(const TEnumAsByte<ECollisionEnabled::Type> CollisionType);

	UFUNCTION(BlueprintCallable)
	virtual void SetCombatCollisionResponse(const TEnumAsByte<ECollisionChannel>& Channel, const TEnumAsByte<ECollisionResponse>& Response);

	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
};
