// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h" 
#include "Components/StaticMeshComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h" 


#include "DrawDebugHelpers.h" 

UWeapon::UWeapon()
{
	// Setup functions needs to be called from every child to be customizable
}

void UWeapon::BeginPlay()
{
	Super::BeginPlay();

	CombatCollision->OnComponentBeginOverlap.AddDynamic(this, &UWeapon::OnOverlapBegin);
	CombatCollision->OnComponentEndOverlap.AddDynamic(this, &UWeapon::OnOverlapEnd);
}

void UWeapon::SetupDefaultSubObject()
{
	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	CombatCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("CombatCollision"));
}

void UWeapon::SetupRootAttachment()
{
	WeaponMesh->SetupAttachment(GetAttachmentRoot());
	CombatCollision->SetupAttachment(GetAttachmentRoot());
}

void UWeapon::SetupCombatCollision()
{
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	CombatCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CombatCollision->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
	
	FCollisionResponseContainer Con = FCollisionResponseContainer(ECollisionResponse::ECR_Ignore);
	Con.SetResponse(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	Con.SetResponse(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Overlap);
	Con.SetResponse(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Overlap);

	CombatCollision->SetCollisionResponseToChannels(Con);

	CombatCollision->SetGenerateOverlapEvents(true);
}

void UWeapon::OverlapResponse(const ECollisionChannel& Response, const FVector& ImpactPoint, const EPhysicalSurface& PhysicalSurface)
{
	PlayCombatParticleSystem(PhysicalSurface, ImpactPoint);
	OnWeaponHitCollision.Broadcast(Response, PhysicalSurface);
}

void UWeapon::PlayCombatParticleSystem(const TEnumAsByte<EPhysicalSurface>& Response, const FVector& ImpactPoint)
{
	if (CollisionParticles.Num() > 0)
	{
		if (CollisionParticles.Contains(Response))
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CollisionParticles[Response], ImpactPoint, CombatCollision->GetComponentRotation(), false, EPSCPoolMethod::AutoRelease);
		}
		else
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CollisionParticles[EPhysicalSurface::SurfaceType_Default], ImpactPoint, CombatCollision->GetComponentRotation(), false, EPSCPoolMethod::AutoRelease);
		}
	}
}

bool UWeapon::OverlapIsValid(AActor* OtherActor, UPrimitiveComponent* OtherComp)
{
	// Override in Child Classes
	return true;
}

void UWeapon::SetCombatCollisionEnabled(const TEnumAsByte<ECollisionEnabled::Type> CollisionType)
{
	CombatCollision->SetCollisionEnabled(CollisionType);
}

void UWeapon::SetCombatCollisionResponse(const TEnumAsByte<ECollisionChannel>& Channel, const TEnumAsByte<ECollisionResponse>& Response)
{
	CombatCollision->SetCollisionResponseToChannel(Channel, Response);
}

void UWeapon::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OverlapIsValid(OtherActor, OtherComp))
	{
		return;
	}

	// Calculate collision point
	FVector OutPoint = FVector::ZeroVector;
	CombatCollision->GetClosestPointOnCollision(OverlappedComponent->GetComponentLocation(), OutPoint);
	
	EPhysicalSurface PhysicalSurface = EPhysicalSurface::SurfaceType_Default;
	if (OtherComp->GetMaterial(0) != nullptr)
	{
		PhysicalSurface  = OtherComp->GetMaterial(0)->GetPhysicalMaterial()->SurfaceType;
	}

	OverlapResponse(OtherComp->GetCollisionObjectType(), OutPoint, PhysicalSurface);
}

void UWeapon::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

