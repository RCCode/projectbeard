// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerShield.h"
#include "Kismet/GameplayStatics.h" 
#include "../Player/PlayerCharacter.h"
#include "../AI/EnemyCharacter.h"
#include "Components/BoxComponent.h" 

UPlayerShield::UPlayerShield()
{
	SetupDefaultSubObject();
	SetupRootAttachment();
	SetupCombatCollision();
}

void UPlayerShield::SetupCombatCollision()
{
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	CombatCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CombatCollision->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);

	FCollisionResponseContainer Con = FCollisionResponseContainer(ECollisionResponse::ECR_Ignore);
	Con.SetResponse(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Overlap);

	CombatCollision->SetCollisionResponseToChannels(Con);

	CombatCollision->SetGenerateOverlapEvents(true);
}

void UPlayerShield::OverlapResponse(const ECollisionChannel& Response, const FVector& ImpactPoint, const EPhysicalSurface& PhysicalSurface)
{
	Super::OverlapResponse(Response, ImpactPoint, PhysicalSurface);
}

bool UPlayerShield::OverlapIsValid(AActor* OtherActor, UPrimitiveComponent* OtherComp)
{
	if (OtherActor == nullptr)
	{
		return false;
	}

	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_Pawn)
	{
		if (Cast<AEnemyCharacter>(OtherActor) == nullptr)
		{
			return false;
		}
	}

	return true;
}

void UPlayerShield::BeginPlay()
{
	Player = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Player)
	{
		Player->PlayerShield = this;
	}
	Super::BeginPlay();
}
