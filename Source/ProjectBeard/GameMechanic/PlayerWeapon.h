// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "PlayerWeapon.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTBEARD_API UPlayerWeapon : public UWeapon
{
	GENERATED_BODY()
	
public:

	UPlayerWeapon();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
	TSubclassOf<UDamageType> DamageType;

protected:

	UPROPERTY()
	class APlayerCharacter* Player;

protected:

	virtual void OverlapResponse(const ECollisionChannel& Response, const FVector& ImpactPoint, const EPhysicalSurface& PhysicalSurface) override;

	virtual bool OverlapIsValid(AActor* OtherActor, UPrimitiveComponent* OtherComp) override;

	virtual void BeginPlay() override;
public:
	

	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

};
