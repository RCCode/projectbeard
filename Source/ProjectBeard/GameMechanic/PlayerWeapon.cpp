// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerWeapon.h"
#include "Kismet/GameplayStatics.h" 
#include "../Player/PlayerCharacter.h"
#include "../AI/EnemyCharacter.h"
#include "GameFramework/DamageType.h" 




UPlayerWeapon::UPlayerWeapon()
{
	SetupDefaultSubObject();
	SetupRootAttachment();
	SetupCombatCollision();
}

void UPlayerWeapon::OverlapResponse(const ECollisionChannel& Response, const FVector& ImpactPoint, const EPhysicalSurface& PhysicalSurface)
{
	Super::OverlapResponse(Response, ImpactPoint, PhysicalSurface);
}

bool UPlayerWeapon::OverlapIsValid(AActor* OtherActor, UPrimitiveComponent* OtherComp)
{
	if (OtherActor == nullptr)
	{
		return false;
	}

	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_Pawn)
	{
		if (Cast<AEnemyCharacter>(OtherActor) == nullptr)
		{
			return false;
		}
	}

	return true;
}

void UPlayerWeapon::BeginPlay()
{
	Player = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (Player)
	{
		Player->PlayerWeapon = this;
	}
	Super::BeginPlay();
}


void UPlayerWeapon::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if (Cast<AEnemyCharacter>(OtherActor))
	{
		FHitResult Hit;
		UGameplayStatics::ApplyPointDamage(OtherActor, Player->CharacterAttributes[ECharacterID::AttackDamage](), GetComponentLocation(), Hit, Player->GetController(), Player, DamageType);
	}
}
