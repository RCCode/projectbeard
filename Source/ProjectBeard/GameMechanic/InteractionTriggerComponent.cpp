// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionTriggerComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/SphereComponent.h"
#include "Blueprint/UserWidget.h"

UInteractionTriggerComponent::UInteractionTriggerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("InteractionWidget2"));
	WidgetComponent->SetupAttachment(GetAttachmentRoot());
	WidgetComponent->SetDrawSize(FVector2D(100.f,100.f));
	WidgetComponent->PrimaryComponentTick.bCanEverTick = false;
	WidgetComponent->PrimaryComponentTick.bStartWithTickEnabled = false;
	SetCollisionProfileName(TEXT("Interactable"));
}

void UInteractionTriggerComponent::BeginPlay()
{
	Super::BeginPlay();
	if (!WidgetComponent->IsRegistered())
	{
		WidgetComponent->RegisterComponent();
	}
	WidgetComponent->SetVisibility(false);
	UpdateCollisionResponsibility();
}

void UInteractionTriggerComponent::UpdateCollisionResponsibility()
{
	if (bUseParentCollision)
	{
		UPrimitiveComponent* temp = Cast<UPrimitiveComponent>(GetAttachParent());
		if(temp)
		{
			if (temp->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
			{
				temp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
			}
			temp->SetCollisionResponseToChannel(
			ECollisionChannel::ECC_GameTraceChannel4, 
			ECollisionResponse::ECR_Block);
			SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}
	}
}

void UInteractionTriggerComponent::Interact()
{
	OnInteraction.Broadcast();
}

void UInteractionTriggerComponent::EnterFocus()
{
	if (WidgetComponent->IsRegistered())
	{
		WidgetComponent->SetVisibility(true);
	}
}

void UInteractionTriggerComponent::ExitFocus()
{
	if (WidgetComponent->IsRegistered())
	{
		WidgetComponent->SetVisibility(false);
	}
}
