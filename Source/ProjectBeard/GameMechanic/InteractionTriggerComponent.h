// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "InteractionTriggerComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInteraction);
/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (DisplayName = "InteractionTrigger", BlueprintSpawnableComponent))
class PROJECTBEARD_API UInteractionTriggerComponent : public USphereComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interaction", meta = (DisplayName = "Use Parent Collision"))
	bool bUseParentCollision = false;

	UPROPERTY(BlueprintAssignable)
	FOnInteraction OnInteraction;

	UPROPERTY(VisibleAnywhere, Category = "Interaction")
	class UWidgetComponent* WidgetComponent;

public:
	UInteractionTriggerComponent();
	virtual void BeginPlay() override;
	void UpdateCollisionResponsibility();
	UFUNCTION(BlueprintCallable)
	void Interact();
	UFUNCTION(BlueprintCallable)
	void EnterFocus();
	UFUNCTION(BlueprintCallable)
	void ExitFocus();
};
