// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "../InterpFloat.h"
#include "PlayerRuneCollector.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRuneDetected, AActor*, RuneActor);

UCLASS(EditInlineNew, Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class PROJECTBEARD_API UPlayerRuneCollector : public USphereComponent
{
	GENERATED_BODY()
	

public:
	
	UPlayerRuneCollector();

	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Rune)
	float LerpSpeed;

	UPROPERTY(BlueprintAssignable)
	FOnRuneDetected OnRuneDetected;

private:

	UPROPERTY()
	class APlayerCharacter* Player;

	UPROPERTY()
	TSet<AActor*> CurrentRunes;

private:

	void IterateCollectedRunes(const float& DeltaTime);
	
	void LerpRune(AActor* CurrentRune,const float& DeltaTime);


protected:

	virtual void BeginPlay() override;

	

public:

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
