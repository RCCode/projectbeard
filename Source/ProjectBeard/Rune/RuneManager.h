// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RuneProperty.h"
#include "RuneManager.generated.h"

/**
 * Manages and holds all runes that exist in the game.
 * Each rune that is used by other classes work with references to this class.
 */
UCLASS(Blueprintable, ClassGroup = (Runes), BlueprintType)
class PROJECTBEARD_API URuneManager : public UObject
{
	GENERATED_BODY()
public:
	/** Holds the only instance of each rune.*/
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta =(EditFixedOrder))
	TArray<FRuneProperty> Runes;

	// Bools used as buttons to trigger functions inside PostEditChangeProperty
	UPROPERTY(EditAnywhere, Category = "Buttons", meta = (DisplayName = "Sort Runes By DropType"))
	bool ButtonSortRunesByDropType = false;
	UPROPERTY(EditAnywhere, Category = "Buttons", meta = (DisplayName = "Remove None Setup Runes"))
	bool ButtonRemoveNoneSetupRunes = false;

private:
	/** 
	* Holds index-markers which determine the range, from where to where
	* runes with the specific EDropCategories are stored inside the 
	* SortedRunes Array.
	*/
	UPROPERTY(VisibleAnywhere)
	TMap<uint8, int16> RuneRarityIndexMarker;
	/** Indices for "Runes" sorted by DropType*/
	UPROPERTY(VisibleAnywhere)
	TArray<int16> SortedRuneIndices;
	UPROPERTY()
	FRandomStream RStream;

// Functions:
public:
	URuneManager();
	~URuneManager();
	void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;

	void FillRuneProperty(TMap<EDropType, int32> RangeTable,class ARune& OutRune);

private:
	EDropType DetermineRarity(TMap<EDropType, int32> RangeTable);
	void DeterminePropertyByRarity(EDropType DropType, FRuneProperty& OutRuneProperty);

	void ResetIndexMarker();
	void SortRunesByDropType();
	void RemoveNoneSetupRunes();
};
