// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuneProperty.h"
#include "Rune.generated.h"

UCLASS()
class PROJECTBEARD_API ARune : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Rune, meta = (ExposeOnSpawn = "true"))
	UMaterialInstance* MaterialInstance;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Rune, meta = (ExposeOnSpawn = "true"))
	FRuneProperty RuneProperty;
	
public:	
	// Sets default values for this actor's properties
	ARune();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	// Components:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UStaticMeshComponent* StaticMesh;
};
