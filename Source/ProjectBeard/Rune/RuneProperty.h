// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../CharacterAttribute.h"
#include "RuneProperty.generated.h"

/**
 * DropType is divide runes into rarity categories.
 */
UENUM(BlueprintType)
enum class EDropType : uint8
{
	None UMETA(DisplayName = "None"),
	Common UMETA(DisplayName = "Common"),
	Uncommon UMETA(DisplayName = "Uncommon"),
	Rare UMETA(DisplayName = "Rare"),
	Epic UMETA(DisplayName = "Epic")
};

/**
 * RuneGroup is used to tag runes what general boost effect they have
 * on the player. (Red = Damage, Blue = Shield, Green = Life, Yellow =
 * Special)
 */
UENUM(BlueprintType)
enum class ERuneGroup : uint8
{
	Red UMETA(DisplayName = "Red"),
	Blue UMETA(DisplayName = "Blue"),
	Green UMETA(DisplayName = "Green"),
	Yellow UMETA(DisplayName = "Yellow")
};

/**
 * Contains all important information that is needed to know what
 * a rune does.
 */
USTRUCT(BlueprintType)
struct PROJECTBEARD_API FRuneProperty
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName RuneName;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ERuneGroup RuneGroup;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EDropType DropType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ECharacterID AffectedAttribute;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ClampMin = 0.0))
	float AttributeBoost;
};
