// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RuneManager.h"
#include "RuneDropper.generated.h"

UCLASS( Blueprintable, ClassGroup=(Runes), meta=(BlueprintSpawnableComponent) )
class PROJECTBEARD_API URuneDropper : public UActorComponent
{
	GENERATED_BODY()

public:
	// Logic
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Rune|Logic")
	TSubclassOf<class ARune> RunePtr;
	// Weights
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Rune|Weight")
	TMap<EDropType, int32> WeightTable;
	// Visuals
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Rune|Visual")
	TMap<ERuneGroup, UMaterialInstance*> MaterialTable;
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Rune|Visual")
	float ZSpawnOffset;

private:
	UPROPERTY()
	FRandomStream RStream;
	UPROPERTY()
	TMap<EDropType, int32> RangeTable;
	UPROPERTY()
	URuneManager* RuneManagerPtr;

public:	
	// Sets default values for this component's properties
	URuneDropper();
	/**
	 * Will returns a ARune if loot was generated else returns nullptr.
	 */
	UFUNCTION(BlueprintCallable)
	ARune* ReturnLoot();
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void DropRune();

private:
	bool ShouldRuneBeDropped();
	ARune* GenerateRuneToDrop();

	void CreateWeightTable();
	void CreateRangeTable();
	void CreateMaterialTable();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


};
