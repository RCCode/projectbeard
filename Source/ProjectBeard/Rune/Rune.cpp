// Fill out your copyright notice in the Description page of Project Settings.


#include "Rune.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ARune::ARune()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Init Components
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RuneStaticMesh"));
	RootComponent = StaticMesh;
	StaticMesh->SetSimulatePhysics(true);

	// Preconfigure Collision
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	StaticMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);

	FCollisionResponseContainer Con = FCollisionResponseContainer(ECollisionResponse::ECR_Ignore);
	Con.SetResponse(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	Con.SetResponse(ECollisionChannel::ECC_WorldDynamic, ECollisionResponse::ECR_Block);
	Con.SetResponse(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	StaticMesh->SetCollisionResponseToChannels(Con);

	StaticMesh->SetGenerateOverlapEvents(true);
}

// Called when the game starts or when spawned
void ARune::BeginPlay()
{
	Super::BeginPlay();

	StaticMesh->SetMaterial(0,MaterialInstance);
}

// Called every frame
void ARune::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

