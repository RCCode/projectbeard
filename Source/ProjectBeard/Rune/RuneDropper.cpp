// Fill out your copyright notice in the Description page of Project Settings.


#include "RuneDropper.h"
#include "Math/RandomStream.h"
#include "Math/TransformNonVectorized.h"
#include "Kismet/GameplayStatics.h"
#include "Rune.h"
#include "../ProjectBeardGameInstance.h"

// Sets default values for this component's properties
URuneDropper::URuneDropper()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	ZSpawnOffset = 1.0;

	// Create Tables
	CreateWeightTable();
	CreateMaterialTable();
}

ARune* URuneDropper::ReturnLoot()
{
	if(!RunePtr)
	{
		UE_LOG(LogTemp, Warning, TEXT("No Runes was set!"))
		return nullptr;
	}

	if (ShouldRuneBeDropped())
	{
		UE_LOG(LogTemp, Warning, TEXT("No Runes were dropped."))
		return nullptr;
	}

	return GenerateRuneToDrop();
}

bool URuneDropper::ShouldRuneBeDropped()
{
	UE_LOG(LogTemp, Warning, 
		TEXT("None: %i; Epic: %i"), 
		WeightTable[EDropType::None],
		RangeTable[EDropType::Epic])
	int32 rNum = RStream.RandRange(
		0,
		WeightTable[EDropType::None] + RangeTable[EDropType::Epic]);
	return (rNum <= WeightTable[EDropType::None]);
}

ARune* URuneDropper::GenerateRuneToDrop()
{
	FTransform spawnTransform = FTransform(
		GetOwner()->GetActorRotation(),
		GetOwner()->GetActorLocation() + FVector(0, 0, ZSpawnOffset));

	ARune* result = GetWorld()->SpawnActorDeferred<ARune>(
		RunePtr,
		spawnTransform,
		nullptr,
		nullptr,
		ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

	if (result)
	{
		RuneManagerPtr->FillRuneProperty(RangeTable, *result);
		result->MaterialInstance = MaterialTable[result->RuneProperty.RuneGroup];
		UGameplayStatics::FinishSpawningActor(result, spawnTransform);
	}

	return result;
}

void URuneDropper::CreateWeightTable()
{
	WeightTable.Add(EDropType::None);
	WeightTable.Add(EDropType::Common);
	WeightTable.Add(EDropType::Uncommon);
	WeightTable.Add(EDropType::Rare);
	WeightTable.Add(EDropType::Epic);
}

void URuneDropper::CreateRangeTable()
{
	RangeTable.Empty();
	RangeTable.Add(
		EDropType::Common,
		WeightTable[EDropType::Common]);
	RangeTable.Add(
		EDropType::Uncommon,
		WeightTable[EDropType::Common] + 
		WeightTable[EDropType::Uncommon]);
	RangeTable.Add(
		EDropType::Rare,
		WeightTable[EDropType::Common] +
		WeightTable[EDropType::Uncommon] + 
		WeightTable[EDropType::Rare]);
	RangeTable.Add(
		EDropType::Epic,
		WeightTable[EDropType::Common] +
		WeightTable[EDropType::Uncommon] +
		WeightTable[EDropType::Rare] + 
		WeightTable[EDropType::Epic]);
}

void URuneDropper::CreateMaterialTable()
{
	MaterialTable.Add(ERuneGroup::Red, nullptr);
	MaterialTable.Add(ERuneGroup::Blue, nullptr);
	MaterialTable.Add(ERuneGroup::Green, nullptr);
	MaterialTable.Add(ERuneGroup::Yellow, nullptr);
}

// Called every frame
void URuneDropper::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

// Called when the game starts
void URuneDropper::BeginPlay()
{
	Super::BeginPlay();

	RuneManagerPtr = Cast<UProjectBeardGameInstance>(GetWorld()->GetGameInstance())->RuneManager();

	CreateRangeTable();
	RStream.Initialize(FMath::Rand());
}