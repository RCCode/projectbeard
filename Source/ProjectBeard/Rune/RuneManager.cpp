// Fill out your copyright notice in the Description page of Project Settings.


#include "RuneManager.h"
#include "Rune.h"
#include "Engine/UserDefinedEnum.h"

#define PB_DROPTYPASBYTE (uint8)EDropType

URuneManager::URuneManager()
{
	// Init RuneRarityIndexMarker with -1
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::None, -1);
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::Common, -1);
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::Uncommon, -1);
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::Rare, -1);
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::Epic, -1);

	// Init RandomStream
	RStream.Initialize(FMath::Rand());
}

URuneManager::~URuneManager()
{
}

void URuneManager::PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent)
{
	FName PropertyName = (PropertyChangedEvent.Property != NULL) ? PropertyChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(URuneManager, ButtonSortRunesByDropType)))
	{
		SortRunesByDropType();
		ButtonSortRunesByDropType = false;
// 		UE_LOG(LogTemp, Warning, TEXT("Manager: Number: %i"),
// 			RarityTable[EDropCategory::Common].GetArray().Num())
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(URuneManager, ButtonRemoveNoneSetupRunes)))
	{
		RemoveNoneSetupRunes();
		ButtonRemoveNoneSetupRunes = false;
		SortRunesByDropType();
	}
}

void URuneManager::FillRuneProperty(TMap<EDropType, int32> RangeTable, ARune& OutRune)
{
	DeterminePropertyByRarity(DetermineRarity(RangeTable), OutRune.RuneProperty);
}

// Private Functions:

EDropType URuneManager::DetermineRarity(TMap<EDropType, int32> RangeTable)
{
	TArray<EDropType> keys;
	RangeTable.GenerateKeyArray(keys);
	int32 rNum = RStream.RandRange(0, RangeTable[keys[keys.Num() - 1]]);
	EDropType result = keys[0];
	for (const TPair<EDropType, int32>& pair : RangeTable)
	{
		if (rNum <= pair.Value)
		{
			result = pair.Key;
			break;
		}
	}
	return result;
}

void URuneManager::DeterminePropertyByRarity(EDropType DropType, FRuneProperty& OutRuneProperty)
{
	int16 from = (uint8)DropType - 1;
	int16 to = (uint8)DropType;
	int16 rNum = RStream.RandRange(
		RuneRarityIndexMarker[from] + 1,
		RuneRarityIndexMarker[to]);
	OutRuneProperty = Runes[SortedRuneIndices[rNum]];
}

void URuneManager::ResetIndexMarker()
{
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::None, -1);
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::Common, -1);
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::Uncommon, -1);
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::Rare, -1);
	RuneRarityIndexMarker.Add(PB_DROPTYPASBYTE::Epic, -1);
}

void URuneManager::SortRunesByDropType()
{
	SortedRuneIndices.Empty();
	ResetIndexMarker();
	for (int16 RuneIndex = 0; RuneIndex < Runes.Num(); RuneIndex++) 
	{
		switch (Runes[RuneIndex].DropType) 
		{
		case EDropType::Common:
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Common]++;
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Uncommon]++;
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Rare]++;
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Epic]++;
			SortedRuneIndices.Insert(
				RuneIndex, 
				RuneRarityIndexMarker[PB_DROPTYPASBYTE::Common]);
			break;
		case EDropType::Uncommon: 
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Uncommon]++;
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Rare]++;
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Epic]++;
			SortedRuneIndices.Insert(
				RuneIndex,
				RuneRarityIndexMarker[PB_DROPTYPASBYTE::Uncommon]);
			break;
		case EDropType::Rare: 
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Rare]++;
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Epic]++;
			SortedRuneIndices.Insert(
				RuneIndex,
				RuneRarityIndexMarker[PB_DROPTYPASBYTE::Rare]);
			break;
		case EDropType::Epic: 
			RuneRarityIndexMarker[PB_DROPTYPASBYTE::Epic]++;
			SortedRuneIndices.Insert(
				RuneIndex,
				RuneRarityIndexMarker[PB_DROPTYPASBYTE::Epic]);
			break;
		case EDropType::None:
			//Stays always at -1 because there are no runes with DropType None.
		default:
			break;
		}
	}
}

void URuneManager::RemoveNoneSetupRunes()
{
	for (int index = 0; index < Runes.Num(); index++)
	{
		if (Runes[index].RuneName.IsNone()) 
		{
			Runes.RemoveAt(index);
			index--;
		}
	}
}