// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Rune/RuneManager.h"
#include "ProjectBeardGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTBEARD_API UProjectBeardGameInstance : public UGameInstance
{
	GENERATED_BODY()

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<URuneManager> RuneManagerInstance;

public:
	UFUNCTION()
	URuneManager* RuneManager();
};
