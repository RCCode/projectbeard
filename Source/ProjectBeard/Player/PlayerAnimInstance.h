// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "../InterpFloat.h"
#include "PlayerAnimInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPlayerHasLanded);

UCLASS()
class PROJECTBEARD_API UPlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:

	// Character is in air
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category  = Movement)
	bool bIsInAir;

	// Only true if the character was in air and has touched the ground
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	bool bHasLanded;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = States)
	bool bCanRotateUpperBody;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = States)
	bool bMontageIsBlendingOut;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Pawn)
	class APawn* Pawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Interpolation)
	FInterpFloat InterpZYaw;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Interpolation)
	FInterpFloat InterpYPitch;

	UPROPERTY(BlueprintAssignable)
	FOnPlayerHasLanded OnPlayerHasLanded;
	

public:

	virtual void NativeInitializeAnimation() override;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

	UFUNCTION(BlueprintImplementableEvent)
	void PlayerHasLanded();

};
