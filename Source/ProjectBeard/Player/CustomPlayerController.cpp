// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomPlayerController.h"
#include "PlayerCharacter.h"
#include "PlayerHUD.h"

ACustomPlayerController::ACustomPlayerController()
{
	PlayerCharacter = Cast<APlayerCharacter>(Player);

}


void ACustomPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	// Call Input for UI Actions
}

void ACustomPlayerController::BeginPlay()
{
	Super::BeginPlay();

	PlayerHUD = Cast<APlayerHUD>(GetHUD());

}
