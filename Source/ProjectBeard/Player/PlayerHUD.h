// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTBEARD_API APlayerHUD : public AHUD
{
	GENERATED_BODY()
	
public:

	APlayerHUD();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Widgets)
	TSubclassOf<class UHUDWidget> HUDWidgetClass;

	UPROPERTY(BlueprintReadOnly, Category = Widgets)
	TObjectPtr<UHUDWidget> HUDWidget;

private:

protected:

	virtual void BeginPlay() override;

public:
	
	UFUNCTION(BlueprintImplementableEvent)
	void PlayerHealthChange(const float& HealthPercentage);
};
