// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerAnimInstance.h"
#include "PlayerCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h" 


void UPlayerAnimInstance::NativeInitializeAnimation()
{

	if (Pawn == nullptr)
	{
		Pawn = TryGetPawnOwner();
	}

	if (!OnPlayerHasLanded.IsBound())
	{
		OnPlayerHasLanded.AddDynamic(this, &UPlayerAnimInstance::PlayerHasLanded);
	}
}

void UPlayerAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	if (Pawn)
	{
		if (bIsInAir && !bHasLanded)
		{
			bHasLanded = true;
		}
		else if (!bIsInAir && bHasLanded)
		{
			OnPlayerHasLanded.Broadcast();
			bHasLanded = false;
		}

		bIsInAir = Pawn->GetMovementComponent()->IsFalling();

		if (bCanRotateUpperBody)
		{
			InterpZYaw.InterpolateAngle(UKismetMathLibrary::NormalizedDeltaRotator(Pawn->GetControlRotation(), Pawn->GetActorRotation()).Yaw, DeltaSeconds);
			InterpYPitch.InterpolateAngle(UKismetMathLibrary::NormalizedDeltaRotator(Pawn->GetControlRotation(), Pawn->GetActorRotation()).Pitch, DeltaSeconds);
		}

	}
	else
	{
		Pawn = TryGetPawnOwner();
	}
}
