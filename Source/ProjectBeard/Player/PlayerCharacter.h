// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../CharacterAttribute.h"
#include "../Rune/RuneProperty.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class PROJECTBEARD_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class USpringArmComponent* CameraSpringArm;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	class UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	class UPlayerWeapon* PlayerWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat)
	class UPlayerShield* PlayerShield;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float BaseTurnRate;

	UPROPERTY(EditDefaultsOnly, Category = Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, Category = Movement)
	float MaxRunningMultiplier;

	UPROPERTY(EditDefaultsOnly, Category = Tick)
	float SlowTickRate;

	UPROPERTY(EditDefaultsOnly, Category = Rune)
	float MaxRuneTatooEmissive;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	FVector2D MovementAxis;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* BlockMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Attributes, meta = (EditFixedOrder = "false"))
	TMap<ECharacterID, FCharacterAttribute> CharacterAttributes;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* AttackMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	UAnimMontage* HitMontage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	TEnumAsByte<EPhysicalSurface> CurrentPhysicalSurface;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Rune)
	TMap<ERuneGroup, UParticleSystem*> RuneCollisionParticles;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Rune)
	TMap<ERuneGroup, float> RuneParameterValues;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<class ACustomPlayerController> PlayerController;

	// TODO make InventoryComponent with FStruct int DropTypeCounter; TMap<int, int> RuneIDWithCounter;
	// TMap<EDropType, TMap<int, int>> RuneRarityCounter;

private:

	UPROPERTY()
	int32 CurrentAttackMontageIndex;

	UPROPERTY()
	float CurrentRunningMultiplier;

	UPROPERTY()
	float CurrentSlowTick;

	UPROPERTY()
	class UPlayerAnimInstance* AnimInstance;

	UPROPERTY()
	bool bIsBlocking;

private:

	void SetupDefaultSubobject();

	void SetupRoot();

	void SetupDefaultValues();

	// Bind InputAction to Key
	void SetupPlayerInputAction(class UInputComponent*& PlayerInputComponent);

	// Bind InputAxis to Key
	void SetupPlayerInputAxis(class UInputComponent*& PlayerInputComponent);

	void MoveCharacter(const float& Value, const EAxis::Type& DirectionAxis);

	void SetMovementSpeed(const float& Speed);

	// Rune change Functions
	void AddMovementMultiplier(const float& AddValue);

	void SlowTick(float& DeltaTime);

	UFUNCTION()
	void CheckGroundSurfaceType();

	void SetAttributesByRune(FRuneProperty& RuneProperty);

	void PlayRuneParticleSystem(const ERuneGroup& RuneGroup,const FVector& Location);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Jump() override;

	virtual void OutsideWorldBounds() override;

	UFUNCTION()
	virtual void PlayerTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	// Montage Delegate Functions
	UFUNCTION()
	virtual void OnPlayerMontageStarted(UAnimMontage* Montage);
	
	UFUNCTION()
	virtual void OnPlayerMontageBlendOut(UAnimMontage* Montage, bool b);

	UFUNCTION()
	virtual void OnPlayerMontageEnd(UAnimMontage* Montage, bool b);

	UFUNCTION()
	virtual void OnMeshOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);



	// Binding Functions
	void MoveFast();
	void StopMoveFast();
	void BlockStart();
	void BlockEnd();
	void AttackStart();

	void MoveForward(float value);
	void MoveRight(float value);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Material)
	void ChangeParameterCollection(const ERuneGroup& ChangedStat);

	UFUNCTION(BlueprintImplementableEvent)
	void OnRuneCollected(const ERuneGroup& RuneGroup, AActor* RuneActor);

	UFUNCTION(BlueprintImplementableEvent)
	void GroundSurfaceTypeChanged(const TEnumAsByte<EPhysicalSurface>& SurfaceType);

};
