// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"
#include "../Widgets/HUDWidget.h"
#include "Kismet/GameplayStatics.h"


APlayerHUD::APlayerHUD()
{
	PrimaryActorTick.bCanEverTick = false;
}

void APlayerHUD::BeginPlay()
{
	if (HUDWidgetClass)
	{
		HUDWidget = CreateWidget<UHUDWidget>(GetWorld(), HUDWidgetClass);
		if (HUDWidget)
		{
			HUDWidget->AddToViewport();
			HUDWidget->SetVisibility(ESlateVisibility::Visible);
			HUDWidget->ShowWidget();
		}
	}

	Super::BeginPlay();

}
