// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CustomPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTBEARD_API ACustomPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	
	ACustomPlayerController();

	UPROPERTY()
	TObjectPtr<class APlayerCharacter> PlayerCharacter;

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<class APlayerHUD> PlayerHUD;

private:
	
	

protected:

	virtual void SetupInputComponent() override;

	virtual void BeginPlay() override;

};
