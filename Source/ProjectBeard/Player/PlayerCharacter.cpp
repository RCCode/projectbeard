// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "PlayerAnimInstance.h"
#include "../CharacterAttribute.h"
#include "../GameMechanic/PlayerWeapon.h"
#include "Engine/World.h" 
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h" 
#include "../GameMechanic/PlayerShield.h"
#include "../Rune/Rune.h"
#include "../Rune/RuneProperty.h"
#include "Math/UnrealMathUtility.h" 
#include "PhysicalMaterials/PhysicalMaterial.h" 
#include "CustomPlayerController.h"
#include "PlayerHUD.h"
#include "../Widgets/HUDWidget.h"



// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetupDefaultSubobject();
	SetupRoot();
	SetupDefaultValues();

}

#pragma region private

void APlayerCharacter::SetupDefaultSubobject()
{
	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
}

void APlayerCharacter::SetupRoot()
{
	CameraSpringArm->SetupAttachment(GetRootComponent());
	Camera->SetupAttachment(CameraSpringArm, USpringArmComponent::SocketName);
}

void APlayerCharacter::SetupDefaultValues()
{
	//Configure Camera
	CameraSpringArm->TargetArmLength = 400;
	CameraSpringArm->TargetOffset.Z = 80;
	CameraSpringArm->bEnableCameraLag = true;
	CameraSpringArm->bEnableCameraRotationLag = true;
	CameraSpringArm->CameraLagSpeed = 50.0f;
	CameraSpringArm->CameraLagMaxDistance = 10.0f;
	CameraSpringArm->bUsePawnControlRotation = true;

	Camera->bUsePawnControlRotation = false;
	BaseTurnRate = 65;
	BaseLookUpRate = 65;

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;
	
	// Set CharacterAttributes for Editor
	// Delete UPROPERTY from Array to regenerate Array with default Values
	float DefaultAtr[] = { 240.0f, 10.f, 10.f, 100.f, 1.0f, 0.04f, 0.12f, 0.1f, 25.f, 0.1f, 0.1f, 0.1f, 1.5f, 0.1f, 0.1f, 0.1f };
	float CurrentAtr[] = { 240.0f, 10.f, 10.f, 100.f, 1.0f, 0.04f, 0.12f, 0.f, 0.f, 0.f, 0.f, 0.1f, 1.5f, 0.f, 0.f, 0.f };

	FCharacterAttribute CharacterAttribute;
	for (int i = 0; i < int(ECharacterID::CharacterIDMAX); i++)
	{
		CharacterAttribute.DefaultAttribute = DefaultAtr[i];
		CharacterAttribute.CurrentAttribute = CurrentAtr[i];
		CharacterAttributes.Add(ECharacterID(i), CharacterAttribute);
	}

	// Configure CharacterMovement
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 320.f, 0.f);
	GetCharacterMovement()->JumpZVelocity = 300;
	GetCharacterMovement()->AirControl = 0.8;
	SetMovementSpeed(CharacterAttributes[ECharacterID::MovementSpeed]());
	MaxRunningMultiplier = 2.0f;
	CurrentRunningMultiplier = 1.0f;
	MovementAxis = FVector2D::ZeroVector;
	
	// Set Collision
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
	GetCapsuleComponent()->SetGenerateOverlapEvents(true);


	// Configure Animation
	CurrentAttackMontageIndex = 0;

	// Configure Tick
	SlowTickRate = 1.000f;
	CurrentSlowTick = 0.f;

	MaxRuneTatooEmissive = 20.f;
	
	for (int i = 0;i <= int(ERuneGroup::Yellow); i++)
	{
		RuneParameterValues.Add(ERuneGroup(i), 0.f);
	}

}

void APlayerCharacter::SetupPlayerInputAction(class UInputComponent*& PlayerInputComponent)
{
	// Move Fast
	PlayerInputComponent->BindAction("MoveFast", IE_Pressed, this, &APlayerCharacter::MoveFast);
	PlayerInputComponent->BindAction("MoveFast", IE_Released, this, &APlayerCharacter::StopMoveFast);

	// Jump
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &APlayerCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Block
	PlayerInputComponent->BindAction("Block", IE_Pressed, this, &APlayerCharacter::BlockStart);
	PlayerInputComponent->BindAction("Block", IE_Released, this, &APlayerCharacter::BlockEnd);

	// Attack
	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &APlayerCharacter::AttackStart);
}

void APlayerCharacter::SetupPlayerInputAxis(class UInputComponent*& PlayerInputComponent)
{
	// Move
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);

	// Turning Camera
	PlayerInputComponent->BindAxis("LookX", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookY", this, &APawn::AddControllerPitchInput);
}

void APlayerCharacter::MoveCharacter(const float& Value, const EAxis::Type& DirectionAxis)
{
	if (Value != 0.0f)
	{
		GetCharacterMovement()->bUseControllerDesiredRotation = true;

		// Get Forward Vector
		const FVector Direction =
		FRotationMatrix
		(
			FRotator(0, Controller->GetControlRotation().Yaw, 0)
		)
		.GetUnitAxis(DirectionAxis);

		SetMovementSpeed(CharacterAttributes[ECharacterID::MovementSpeed]());


		AddMovementInput(Direction, Value);
		
	}
	else if(GetVelocity().Size2D() == 0)
	{
		GetCharacterMovement()->bUseControllerDesiredRotation = false;
	}
}

void APlayerCharacter::SetMovementSpeed(const float& Speed)
{
	GetCharacterMovement()->MaxWalkSpeed = Speed * CurrentRunningMultiplier;

}

void APlayerCharacter::SlowTick(float& DeltaTime)
{
	if (CurrentSlowTick < SlowTickRate)
	{
		CurrentSlowTick += DeltaTime;
		return;
	}

	CurrentSlowTick = 0.f;
	
	// Call Slow Tick Functions

	if (!AnimInstance->bIsInAir)
	{
		CheckGroundSurfaceType();
	}
}

void APlayerCharacter::CheckGroundSurfaceType()
{
	FHitResult outHit;
	FVector EndVector = GetActorLocation() - FVector(0.f, 0.f, 200.f);
	FCollisionQueryParams GroundSurfaceQueryParams = FCollisionQueryParams::DefaultQueryParam;
	GroundSurfaceQueryParams.bReturnPhysicalMaterial = true;
	GroundSurfaceQueryParams.AddIgnoredActor(this);
	
	if (GetWorld()->LineTraceSingleByChannel(outHit, GetActorLocation(), EndVector, ECollisionChannel::ECC_Visibility, GroundSurfaceQueryParams))
	{
		if (CurrentPhysicalSurface != UGameplayStatics::GetSurfaceType(outHit))
		{
			CurrentPhysicalSurface = UGameplayStatics::GetSurfaceType(outHit);
			GroundSurfaceTypeChanged(CurrentPhysicalSurface);
		}
	}
}

void APlayerCharacter::SetAttributesByRune(FRuneProperty& RuneProperty)
{
	CharacterAttributes[RuneProperty.AffectedAttribute].UpdateCurrentAttribute(RuneProperty.AttributeBoost);

	PlayerController->PlayerHUD->HUDWidget->ChangeNewPickUp(FName(FString::FromInt(int(RuneProperty.AttributeBoost * 100)) + " %"), RuneProperty.RuneName, RuneProperty.RuneGroup);

	if (!RuneParameterValues.IsEmpty())
	{
		RuneParameterValues[RuneProperty.RuneGroup] += MaxRuneTatooEmissive * 0.001;
		RuneParameterValues[RuneProperty.RuneGroup] = FMath::Clamp(RuneParameterValues[RuneProperty.RuneGroup], 0.f, MaxRuneTatooEmissive);
	}

	ChangeParameterCollection(RuneProperty.RuneGroup);
}

void APlayerCharacter::PlayRuneParticleSystem(const ERuneGroup& RuneGroup, const FVector& Location)
{
	if (RuneCollisionParticles.Num() <= 0 || !RuneCollisionParticles.Contains(RuneGroup))
	{
		return;
	}

	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), RuneCollisionParticles[RuneGroup], Location, FRotator::ZeroRotator, false, EPSCPoolMethod::AutoRelease);
}

#pragma endregion
// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	
	PlayerController = Cast<ACustomPlayerController>(GetController());
	AnimInstance = Cast<UPlayerAnimInstance>(GetMesh()->GetAnimInstance());

	AnimInstance->OnPlayerHasLanded.AddDynamic(this, &APlayerCharacter::CheckGroundSurfaceType);

	Super::BeginPlay();

	AnimInstance->OnMontageEnded.AddDynamic(this, &APlayerCharacter::OnPlayerMontageEnd);
	AnimInstance->OnMontageBlendingOut.AddDynamic(this, &APlayerCharacter::OnPlayerMontageBlendOut);
	AnimInstance->OnMontageStarted.AddDynamic(this, &APlayerCharacter::OnPlayerMontageStarted);
	OnTakeAnyDamage.AddDynamic(this, &APlayerCharacter::PlayerTakeAnyDamage);

	GetMesh()->OnComponentBeginOverlap.AddDynamic(this, &APlayerCharacter::OnMeshOverlapBegin);

	CharacterAttributes[ECharacterID::CurrentHealth].CurrentMultiplier = CharacterAttributes[ECharacterID::CurrentHealth].CurrentAttribute / CharacterAttributes[ECharacterID::MaxHealth].CurrentAttribute;
	PlayerController->PlayerHUD->PlayerHealthChange(CharacterAttributes[ECharacterID::CurrentHealth].CurrentMultiplier);

	// Load Data
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SlowTick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	SetupPlayerInputAction(PlayerInputComponent);
	SetupPlayerInputAxis(PlayerInputComponent);

}

void APlayerCharacter::Jump()
{
	Super::Jump();
}

void APlayerCharacter::OutsideWorldBounds()
{
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->RestartLevel();
}

void APlayerCharacter::PlayerTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (!DamageCauser)
	{
		return;
	}


	// Apply Damage
	CharacterAttributes[ECharacterID::CurrentHealth].CurrentAttribute -= Damage;
	CharacterAttributes[ECharacterID::CurrentHealth].CurrentMultiplier = CharacterAttributes[ECharacterID::CurrentHealth].CurrentAttribute / CharacterAttributes[ECharacterID::MaxHealth].CurrentAttribute;

	PlayerController->PlayerHUD->PlayerHealthChange(CharacterAttributes[ECharacterID::CurrentHealth].CurrentMultiplier);


	// Calculate Animation

	float resDot = FVector::DotProduct(GetActorForwardVector(), DamageCauser->GetActorForwardVector());

	if (AnimInstance->Montage_IsPlaying(BlockMontage))
	{
		if (Damage >= CharacterAttributes[ECharacterID::BlockValue]())
		{
			AnimInstance->Montage_JumpToSection(TEXT("ShieldHitHeavy"));
		}
		else
		{
			if (resDot < 0.f)
			{
				AnimInstance->Montage_JumpToSection(TEXT("ShieldHitFrontal"));
			}
			else
			{
				AnimInstance->Montage_JumpToSection(TEXT("ShieldHitBack"));
			}
		}
	}
	else if (HitMontage)
	{
		AnimInstance->Montage_Play(HitMontage);
		if (resDot > 0.25f)
		{
			AnimInstance->Montage_JumpToSection(TEXT("Back"));
		}
		else
		{
			if (resDot < -0.25f)
			{
				AnimInstance->Montage_JumpToSection(TEXT("Frontal"));
			}
			else
			{
				resDot = FVector::DotProduct(GetActorRightVector(), DamageCauser->GetActorRightVector());

				if (resDot < 0.f)
				{
					AnimInstance->Montage_JumpToSection(TEXT("Right"));
				}
				else
				{
					AnimInstance->Montage_JumpToSection(TEXT("Left"));
				}
			}
		}
	}
}

void APlayerCharacter::OnPlayerMontageStarted(UAnimMontage* Montage)
{
	AnimInstance->bMontageIsBlendingOut = false;
}


void APlayerCharacter::OnPlayerMontageBlendOut(UAnimMontage* Montage, bool b)
{
	if (Montage == AttackMontage)
	{
		AnimInstance->bCanRotateUpperBody = false;
	}
	AnimInstance->bMontageIsBlendingOut = true;
}

void APlayerCharacter::OnPlayerMontageEnd(UAnimMontage* Montage, bool b)
{

	// Reset Animation values
	if (Montage == AttackMontage)
	{
		PlayerWeapon->SetCombatCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void APlayerCharacter::OnMeshOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (!OtherActor)
	{
		return;
	}
	
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel1)
	{
		ARune* CurrentRune = Cast<ARune>(OtherActor);

		if (CurrentRune)
		{
			// Change Attributes by Rune
			SetAttributesByRune(CurrentRune->RuneProperty);

			// Call Sound Collected
			OnRuneCollected(CurrentRune->RuneProperty.RuneGroup, OtherActor);

			// Call Particlesystem
			PlayRuneParticleSystem(CurrentRune->RuneProperty.RuneGroup, CurrentRune->GetActorLocation());
			// Destroy Object
			CurrentRune->Destroy();
		}
	}
}

void APlayerCharacter::MoveFast()
{
	CurrentRunningMultiplier = MaxRunningMultiplier;
}

void APlayerCharacter::StopMoveFast()
{
	CurrentRunningMultiplier = 1.0f;
}

void APlayerCharacter::BlockStart()
{
	// Start Montage
	if (BlockMontage)
	{
		AnimInstance->Montage_Play(BlockMontage);
		AnimInstance->Montage_JumpToSection(FName("StartBlock"), BlockMontage);
		AnimInstance->bCanRotateUpperBody = true;
		bIsBlocking = true;
	}
	
	// Enable Shield Collision
}

void APlayerCharacter::BlockEnd()
{
	if (BlockMontage)
	{
		if (AnimInstance->Montage_IsPlaying(BlockMontage))
		{
			AnimInstance->Montage_JumpToSection(FName("EndBlock"), BlockMontage);
			AnimInstance->bCanRotateUpperBody = false;
			bIsBlocking = false;
		}
		PlayerShield->SetCombatCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void APlayerCharacter::AttackStart()
{
	if (AttackMontage)
	{
		if (AnimInstance->Montage_IsPlaying(AttackMontage) && !AnimInstance->bMontageIsBlendingOut)
		{
			return;
		}
		else if(AttackMontage->IsValidSectionIndex(CurrentAttackMontageIndex))
		{
			AnimInstance->Montage_Play(AttackMontage, CharacterAttributes[ECharacterID::AttackSpeed]());
			AnimInstance->Montage_JumpToSection(AttackMontage->GetSectionName(CurrentAttackMontageIndex));
			AnimInstance->bCanRotateUpperBody = true;

			// Cycle through all AnimationSections
			CurrentAttackMontageIndex += 1;
			CurrentAttackMontageIndex = (CurrentAttackMontageIndex % (AttackMontage->GetSectionIndex("AttackEnd") + 1));
		}
	}
}

void APlayerCharacter::MoveForward(float value)
{
	MoveCharacter(value, EAxis::X);
	MovementAxis.X = value;
}

void APlayerCharacter::MoveRight(float value)
{
	MoveCharacter(value, EAxis::Y);
	MovementAxis.Y = value;
}

