// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/NumericLimits.h" 
#include "Math/UnrealMathUtility.h" 
#include "CharacterAttribute.generated.h"

UENUM(BlueprintType)
enum class ECharacterID : uint8
{
	MovementSpeed		UMETA(DisplayName = "MovementSpeed"),
	AttackDamage		UMETA(DisplayName = "AttackDamage"),
	BlockValue			UMETA(DisplayName = "BlockValue"),
	MaxHealth			UMETA(DisplayName = "MaxHealth"),
	AttackSpeed			UMETA(DisplayName = "AttackSpeed"),
	HealStoneAmount		UMETA(DisplayName = "HealStoneAmount"),
	HealStoneChance		UMETA(DisplayName = "HealStoneChance"),
	LifeSteal			UMETA(DisplayName = "LifeSteal"),
	GodShield			UMETA(DisplayName = "GodShield"),
	BleedingDamage		UMETA(DisplayName = "BleedingDamage"),
	Weakening			UMETA(DisplayName = "Weakening"),
	CriticalChance		UMETA(DisplayName = "CriticalChance"),
	CriticalDamage		UMETA(DisplayName = "CriticalDamage"),
	BleedingResistance	UMETA(DisplayName = "BleedingResistance"),
	WeakResistance		UMETA(DisplayName = "WeakResistance"),
	EliteDamage			UMETA(DisplayName = "EliteDamage"),
	CurrentHealth		UMETA(DisplayName = "CurrentHealth"),
	CharacterIDMAX		UMETA(DisplayName = "CharacterIDMAX")
};

USTRUCT(BlueprintType)
struct PROJECTBEARD_API FCharacterAttribute
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float DefaultAttribute = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float CurrentAttribute = 0.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MaxAttribute = MAX_flt;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float CurrentMultiplier = 1.0f;

	float& operator -=(const float& i)
	{
		return UpdateCurrentAttribute(-i);
	}

	float& operator +=(const float& i)
	{
		return UpdateCurrentAttribute(i);
	}

	float& operator ()()
	{
		return CurrentAttribute;
	}

	float& UpdateCurrentAttribute(const float& AdditiveMultiplier)
	{
		
		CurrentMultiplier += AdditiveMultiplier;
		CurrentMultiplier = FMath::Clamp(CurrentMultiplier, 0.0f, MaxAttribute / DefaultAttribute);
		CurrentAttribute = DefaultAttribute * CurrentMultiplier;

		return CurrentAttribute;
	}
};
