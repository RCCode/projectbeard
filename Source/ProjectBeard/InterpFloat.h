// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/UnrealMathUtility.h" 
#include "InterpFloat.generated.h"


USTRUCT(BlueprintType)
struct PROJECTBEARD_API FInterpFloat
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float TargetValue = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float TargetAmount = 50.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float InterpSpeed = 2.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MinValue = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MaxValue = 1.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float ResultValue = 0.0f;

	float& Interpolate(const float& CurrentValue, const float& DeltaSeconds, float Rate = 1.0f)
	{
		if (Rate != 0)
		{
			if (!FMath::IsNearlyEqual(TargetValue, ResultValue, 0.1f))
			{
				ResultValue = FMath::FInterpTo(CurrentValue, TargetValue, DeltaSeconds, InterpSpeed);
			}
			else
			{
				TargetValue = CurrentValue;
				TargetValue += Rate * TargetAmount;
			}
			if (!FMath::IsNearlyEqual(TargetValue, CurrentValue, 1.f))
			{
				TargetValue = CurrentValue;
			}
		}

		return ResultValue;
	}

	float& InterpolateAngle(const float& CurrentValue, const float& DeltaSeconds, float Rate = 1.0f)
	{
		if (Rate != 0)
		{
			if (!FMath::IsNearlyEqual(TargetValue, ResultValue, 0.1f))
			{
				ResultValue = FMath::FInterpTo(ResultValue, TargetValue, DeltaSeconds, InterpSpeed);
				ResultValue = FMath::ClampAngle(ResultValue, MinValue, MaxValue);
			}
			else
			{
				TargetValue = CurrentValue;
				TargetValue += Rate * TargetAmount;
			}
			if (!FMath::IsNearlyEqual(TargetValue, CurrentValue, 1.f))
			{
				TargetValue = CurrentValue;
			}
		}

		return ResultValue;
	}

};
