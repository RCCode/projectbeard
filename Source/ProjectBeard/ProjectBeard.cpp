// Copyright Epic Games, Inc. All Rights Reserved.

#include "ProjectBeard.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ProjectBeard, "ProjectBeard" );
