// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectBeardGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTBEARD_API AProjectBeardGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
